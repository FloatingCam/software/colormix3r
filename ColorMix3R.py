#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#    ColorMix3R.xpy, Designed for the Cyclops (2-in, 1-out)
#    and Diamond (3-in, 1-out) 3D-Print heads.
#
#    A post-processing script written in Python.  It adds color mixing
#    control to in a batch (from the command line).  In Slic3r the
#    post-processing can be integrated seamlessly.
#
#    Copyright 2019 Floating Cameras, LLC - Author: Marc Cole
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
import re
import argparse
import math
import os

from pathlib import PurePath


# <editor-fold desc="Globals...">
FINDALL_MIX_RE = re.compile("^\s*;\s*((colorwheel_|tool_|retract_length|layer_height|LAYER_COUNT).*)\s*[=:](.*)$")
RELATIVE_MIX_RE = re.compile("(\d+\.*\d*)/(\d+\.*\d*)")
MIXES_RE = re.compile("(\d+)/(\d+)/*(\d+)*")

VALUES_RE = re.compile("^\s*;\s*(.*)\s*[=:](.*)")
RETRACT_RE = re.compile("\s*E-(\d+\.*\d*)")
UNRETRACT_RE = re.compile("\s*E(\d+\.*\d*)")
LAYER_NUM_RE = re.compile("^\s*;\s*LAYER:\s*(\d+)\s*$")
# </editor-fold desc="Globals...">


class ColorBand:
    def __init__(self,
                 pi_layer_count: int,
                 pi_pattern_idx: int,
                 pi_start_layer: int,
                 pf_layer_height: float = 0.20):
        self.__layer_height = pf_layer_height
        self.pattern_idx = pi_pattern_idx
        self.start_layer = pi_start_layer
        self.height = round(pi_layer_count * self.__layer_height, 2)

        self.__layer_count = pi_layer_count

    # <editor-fold desc="ColorBand Properties and setters...">
    @property
    def end_layer(self):
        """
        Read-only calculated value

        :return: The end layer number
        """
        return self.start_layer + self.layer_count - 1

    @property
    def end_height(self):
        """
        Read-oly calculated value

        :return: Build height in mm
        """
        return round(self.actual_height + self.start_height, 2)

    @property
    def start_height(self):
        """
        Read-oly calculated value

        :return: Starting height in mm for this band
        """
        return round((self.start_layer + 1) * self.__layer_height, 2)

    @property
    def layer_count(self):
        """
        Assure value is set since it is often unknown at instance init

        :return: Number of layers in this band
        """
        if self.__layer_count < 0.0:
            self.__layer_count = int(self.height / self.__layer_height)  # Rounds down to layer_height
        return self.__layer_count

    @layer_count.setter
    def layer_count(self, val):
        """
        Keep height and layer_count in sync.
        :param val:
        """
        self.__layer_count = val
        self.height = round(self.__layer_count * self.__layer_height, 2)

    @property
    def actual_height(self):
        """
        Read-oly calculated value
        :return: Build height based on layer_height which could be slightly smaller if
                 height was set instead of layer_count
        """
        return round(self.layer_count * self.__layer_height, 2)
    # </editor-fold desc="ColorBand Properties and setters...">


class EMix:
    def __init__(self, pa_mix: list, pi_tool_num: int = 99):
        self.tool_num = pi_tool_num
        self.__mix = pa_mix
        self.__mixp = EMix.list_to_mix(pa_mix)
        self.__ecount = len(self.__mixp)

    # <editor-fold desc="EMix Properties and setters...">
    @property
    def mix(self):
        """
        Read-only

        :return: Mix value from import
        """
        return self.__mix

    @property
    def emix(self):
        """
        Read-only

        :return: Percent mix value
        """
        return self.__mixp

    @property
    def extruders(self):
        """
        Read-only

        :return: Number of extruders based on mix (2 or 3 values)
        """
        return self.__ecount
    # </editor-fold desc="EMix Properties and setters...">

    # <editor-fold desc="EMix Methods...">
    def second_retract(self, retract_length: float, comment: str = ""):
        """
        Read-only - Based on the number of filaments retractions have to be 'reversed'.
        This method returns the gcode for the retraction
        or unretraction if the retract_length is negative

        :param retract_length:
        :param comment: Default = ""
        :return: String with the GCode for the given retraction
        """
        return "G1 E{:.2f} ; {}\n".format(retract_length * (self.__ecount - 1), comment)

    def m165(self, comment: str = "Set colormix"):
        """
        Read-only

        :param comment: Default = "Set colormix"
        :return: String with the GCode to set the color mix to this mixes values
        """
        gcode = "M165"
        for i in range(0, len(self.__mixp)):
            gcode += " {}{}".format(chr(65 + i), self.__mixp[i])

        return format("{} ; {}\n").format(gcode, comment)

    def m164(self, pi_tool_id: int = -1):
        """
        Read-only - Setting the mixing values of a virtual extruder
        requires M163 and M164 commands.

        :param pi_tool_id: Typically  0 to 15 the number of virtual extruders is defined in the firmware (Marlin)
        :return: String with the GCode to set the virtual tool values.
        """
        if pi_tool_id < 0:
            pi_tool_id = self.tool_num

        gcode = ""
        for i in range(0, self.__ecount):
            gcode += "M163 S{} P{:.2f}\n".format(i, self.__mixp[i])

        return gcode + "M164 S{0} ; Tool T{0} \n".format(pi_tool_id)

    # </editor-fold desc="EMix Methods...">

    # <editor-fold desc="EMix Static Methods...">
    @staticmethod
    def list_to_mix(inlist: list):
        """
        Given a list that may have empty values convert the elements to float

        Used by Class EMix

        :param inlist:
        :return:

        """
        mixp = []  # initialize as empty list
        for i in range(0, len(inlist)):
            if inlist[i]:
                mixp.append(float(inlist[i]) / 100)
            else:
                break  # Stop looking at first empty

        return mixp
    # </editor-fold desc="EMix Static Methods...">


class RunValues:
    def __init__(self):
        self.val_dic: dict = {}
        self.cycles: int = 1
        self.mode: int = 0
        self.base_height: float = 0.0
        self.rim_height: float = 0.0
        self.layer_height: float = 0.20

        self.__build_height: float = 0.0
        self.__layer_count: int = 0

        self.__mix_values: str = "100/0/0"
        self.__mixes: list = RunValues.parse_mixes(self.__mix_values)

        self.__pattern_values: str = "0"
        self.__pattern = list(map(int, self.__pattern_values))

        self.__colorname_values: str = ""
        self.__colornames: list = list(self.__colorname_values)

        self.__toolmix_values = None
        self.__toolmixes = None

        self.__retract_length: float = -0.01
        self.layer_num: int = 0
        self.color_bands = [ColorBand(pi_layer_count=1000,
                                      pi_pattern_idx=0,
                                      pi_start_layer=0,
                                      pf_layer_height=self.layer_height)]
        self.cur_layer = -1
        self.line_count = 0
        self.__equal_mix_cmd = ""
        self.__has_color = False

    # <editor-fold  desc="RunValue Properties and Setters..."
    @property
    def has_color(self):
        """
        Read-only - Flag set when color mixing values is seen by the process_if_present() method

        :return: True if colorwheel_mixes has been processed (ie color information exists)
        """
        return self.__has_color

    @property
    def retract_length(self):
        """
        This value may not be available until after Layer 0 has been seen and a retraction has occurred.
        A value of -0.01 (less than zero) to flag it's state.  This assures 0.0 is returned instead of
        that flag if the value has not been set.

        :return: float with length specified for retractions or 0.0 if a value hasn't been set
        """
        return self.__retract_length if self.__retract_length >= 0.0 else 0.0

    @retract_length.setter
    def retract_length(self, val: float):
        """
        Retract_length is not always available at instance init so a property exists
        to provide a default value until a value has been set.  A setter is required when
        a property is used.

        :param val:
        """
        if self.__retract_length != val:
            self.__retract_length = val

    @property
    def toolmixes(self):
        """
        Read-only - Set by toolmix_values

        :return: List of EMix Tools if defined by "; tool_mixes = ...""
        """
        return self.__toolmixes

    @property
    def toolmix_values(self):
        """
        Maintain list of toolmixes when toolmix_values is set

        :return: String of values for tool mixes (unparsed)
        """
        return self.__toolmix_values

    @toolmix_values.setter
    def toolmix_values(self, val: str):
        """
        Parses the values provided in a "; tool_mixes = ..." setting

        :param val:

        """
        if self.__toolmix_values != val:
            # self.__has_color = True
            self.__toolmix_values = val.replace(" ", "")
            self.__toolmixes = RunValues.parse_mixes(self.__toolmix_values, is_tool=True)

    @property
    def mixes(self):
        """
        Read-only - Set by mix_values setter

        :return: List of EMix objects, one for each color mix value
        """
        return self.__mixes

    @property
    def mix_values(self):
        """
        Maintains list of color mixes used to create the list of EMIx values (mixes)

        :return: String of color mix values provide in a "; colorwheel_mixes = ..." setting
        """
        return self.__mix_values

    @mix_values.setter
    def mix_values(self, val: str):
        """
        Parses the values provided in a "; colorwheel_mixes = ..." setting and
        flags that color information is available

        :param val:

        """
        if self.__mix_values != val:
            self.__mix_values = val.replace(" ", "")
            self.__mixes = RunValues.parse_mixes(self.__mix_values)
            self.__has_color = True

    @property
    def pattern(self):
        """
        Read-only list of index values to the color mixes.  The pattern controls
        the application of the colormixes.  This is the parsed results set by
        "; colorwheel_pattern = ..." setting.

        :return: List of index values to the mixes list
        """
        return self.__pattern

    @property
    def pattern_values(self):
        """
        Maintain the pattern list when a pattern value from "; colorwheel_pattern = ..." is processed

        :return: Value from the "; colorwheel_pattern = ..." setting
        """
        return self.__pattern_values

    @pattern_values.setter
    def pattern_values(self, val: str):
        """
        Parses the values provided in a "; colorwheel_pattern = ..." setting to a list of integers that
        maps to the mixes list.  This pattern is repeated '; colorwheel_cycles = n" setting, n times.

        :param val:
        """
        self.__pattern_values = val.replace(" ", "")
        self.__pattern = list(map(int, self.__pattern_values.split(",")))

    @property
    def colornames(self):
        """
        List of colorname values if provided in a "; colorwheel_colors = ..." setting

        :return: String of colornames
        """
        return self.__colornames

    @property
    def colorname_values(self):
        """
        Parses string to list of colornames in "; colorwheel_colornames = ..." setting is provided.

        :return: String of color names
        """
        return self.__colorname_values

    @colorname_values.setter
    def colorname_values(self, val: str):
        """
        Parses list of color names provided in "; colorwheel_colornames = ...".

        :param val:

        """
        if self.__colorname_values != val:
            self.__colorname_values = val.strip()
            self.__colornames = list(self.__colorname_values.split(","))

    @property
    def build_height(self):
        """
        Height of the build.

        :return: Build height either calculated or as specified.
        """
        return self.__build_height

    @build_height.setter
    def build_height(self, val: float):
        """
        Layer Count can be set by specifying a height.  When this is done the number of layers
        in the build is set by calculation.  This means the actual_height, which is based on
        layer_count * layer_height might be slightly smaller because the printer cannot print
        a partial layer and the logic rounds down.

        :param val:
        """
        if self.__build_height != val:
            self.__build_height = val
            self.__layer_count = int(self.__build_height / self.layer_height)

    @property
    def layer_count(self):
        """
        Maintains sync between layer_count and build_height

        :return: Number of layers in the build
        """
        return self.__layer_count

    @layer_count.setter
    def layer_count(self, val: int):
        """
        Maintains sync between layer_count and build_height.  When a new
        value is provided the build_height will be updated as well.

        :param val:
        """
        if self.__layer_count != val:
            self.__layer_count = val
            self.__build_height = self.__layer_count * self.layer_height
            # BUILD.layer_count = self.__layer_count

    @property
    def stack_report(self):
        """
        Generates a table of colormix values and the layers they apply to.

        :return: String containing the report.
        """
        rval = ";{:>7} | {:^6} | {:^12} | {:^12}\n".format("Layer#", "Height", "Color Name", "Mix")
        template = ";{:7d} | {:6.2f} | {:^12} | {:}\n"
        for band in self.color_bands:
            rval += \
                template.format(band.start_layer, band.start_height,
                                self.colornames[self.pattern[band.pattern_idx]],
                                self.mixes[self.pattern[band.pattern_idx]].emix)
        rval += \
            template.format(self.layer_count, self.build_height, "", "")

        return rval
    # </editor-fold  desc="RunValue Properties and Setters...">

    # <editor-fold  desc="RunValue Methods..."

    def pattern_index(self, layer_num: int):
        """
        Given a layer return the pattern_index value to the mixes list from the
        color_bands.

        :param layer_num:
        :return: The index of the mixes array for the specified layer
        """
        if layer_num < 0:
            layer_num = 0

        if layer_num > self.layer_count:
            layer_num = self.layer_count
        colorband = next(item for item in self.color_bands
                         if item.start_layer <= layer_num <= item.end_layer)

        return colorband.pattern_idx

    def generate_stack(self):
        """
        Create the 'stack' of ColorBands from the Pattern / Mixes / Cycles / Mode values

        :return: False when the number of layers is unknown
        """
        if self.build_height <= 0.0:
            return False

        base_layers = math.floor(self.base_height / self.layer_height)
        rim_layers = math.floor(self.rim_height / self.layer_height)
        print_layers = self.layer_count - base_layers - rim_layers

        band_layers = int(math.floor(math.ceil(print_layers / self.cycles) / len(self.pattern)))
        cycle_layers = band_layers * len(self.pattern)
        actual_layers = cycle_layers * self.cycles

        delta_layers = (print_layers - actual_layers)  # * self.cycles

        actual_base_layers = int(base_layers + math.ceil(delta_layers / 2))
        # actual_rim_layers = int(rim_layers + math.floor(delta_layers / 2))

        gen_layer_count = actual_base_layers - 1  # Initialize starting layer
        cycle_dir = 1  # Initialize going forward
        j = 0  # Init - Used to capture final j value

        self.color_bands = [ColorBand(pi_layer_count=actual_base_layers,
                                      pi_pattern_idx=0,
                                      pi_start_layer=0)]  # Initialize the list
        for cycle in range(0, self.cycles):
            if cycle_dir > 0:
                for j in range(0, len(self.pattern)):
                    self.color_bands.append(ColorBand(pi_layer_count=band_layers,
                                                      pi_pattern_idx=j,
                                                      pi_start_layer=gen_layer_count + 1,
                                                      pf_layer_height=self.layer_height))
                    gen_layer_count += band_layers

            else:
                for j in range(len(self.pattern) - 1, -1, -1):
                    self.color_bands.append(ColorBand(pi_layer_count=band_layers,
                                                      pi_pattern_idx=j,
                                                      pi_start_layer=gen_layer_count + 1,
                                                      pf_layer_height=self.layer_height))
                    gen_layer_count += band_layers

            if self.mode == 1:  # and self..cycles > 1:
                cycle_dir = -cycle_dir

        self.color_bands.append(ColorBand(pi_layer_count=self.layer_count - gen_layer_count,
                                          pi_pattern_idx=j,
                                          pi_start_layer=gen_layer_count + 1,
                                          pf_layer_height=self.layer_height))

        return True

    def m165(self, pi_layer: int, comment="Set colormix"):
        """
        Color mix for a layer

        :param pi_layer:
        :param comment:
        :return: String of GCode setting the colormix
        """
        return self.__mixes[self.pattern[self.pattern_index(pi_layer)]].m165(comment=comment)

    def m165_equal(self, comment="Equal colormix for retraction"):
        """
        GCode for setting each extruder to the same value to prepare
        for retraction.

        :param comment: Default = "Equal colormix for retraction"
        :return: String of GCode for setting the colormix
        """
        gcode = "M165 "
        dist: float = 100.0 / self.mixes[0].extruders
        if self.mixes[0].extruders == 2:
            gcode += "A{0:.2f} B{0:.2f} ; {1:}\n"
        else:
            gcode += "A{0:.6f} B{0:.6f} C{0:.6f} ; {1:}\n"

        return gcode.format(dist, comment)

    def load_if_present(self,
                        names: list = None):
        """
        Scan for a list of value pairs in the dictionary and load into
        the RunValues if possible.

        :param names: List of names to process
        :return: True if mixes have been defined, False if color is not available
        """

        rval = False
        if names is None:
            names = ["retract_length",
                     "LAYER_HEIGHT",
                     "LAYER_COUNT",
                     "colorwheel_baseheight",
                     "colorwheel_rimheight",
                     "colorwheel_cycles",
                     "colorwheel_mode",
                     "colorwheel_mixes",
                     "colorwheel_pattern",
                     "colorwheel_colornames",
                     "tool_mixes"
                     ]
        for name in names:
            if name not in self.val_dic:
                continue  # Continue on to the next name

            if name == "retract_length":
                self.retract_length = float(self.val_dic["retract_length"])

            elif name == "LAYER_HEIGHT":
                self.layer_height = float(self.val_dic["LAYER_HEIGHT"])

            elif name == "LAYER_COUNT":
                self.layer_count = int(self.val_dic["LAYER_COUNT"])

            elif name == "colorwheel_baseheight":
                self.base_height = float(self.val_dic["colorwheel_baseheight"])

            elif name == "colorwheel_rimheight":
                self.rim_height = float(self.val_dic["colorwheel_rimheight"])

            elif name == "colorwheel_cycles":
                self.cycles = int(self.val_dic["colorwheel_cycles"])

            elif name == "colorwheel_mode":
                self.mode = int(self.val_dic["colorwheel_mode"])

            elif name == "colorwheel_mixes":
                self.mix_values = self.val_dic["colorwheel_mixes"].replace(" ", "")
                rval = True  # Must have color values to proceed

            elif name == "colorwheel_pattern" in self.val_dic:
                self.pattern_values = self.val_dic["colorwheel_pattern"].replace(" ", "")

            elif name == "colorwheel_colornames" in self.val_dic:
                self.colorname_values = self.val_dic["colorwheel_colornames"]

            elif name == "tool_mixes" in self.val_dic:
                self.toolmix_values = self.val_dic["tool_mixes"].replace(" ", "")

        return rval

    # </editor-fold  desc="RunValue Methods..."

    # <editor-fold  desc="RunValue Static Methods..."

    @staticmethod
    def parse_mixes(template: str, is_tool=False):
        """
        Given a string of two or three values delimited with / and
        separated with something other than spaces which are removed.
        The number of value sets establishes the number of Extruders
        when use with the EMix class.
        or
        For example:

         RunValues.parse_mixes("10/90,40/60,0/100,85/15")

         RunValues.parse_mixes("10/10/80 - 0/100/0 - 33/33/34 - 100/0/0 - 10/30/60")



        :param template: two or three values delimited with '/'
        :param is_tool: True assigns assigns a tool ID 0-n (Typically there are 16 virtual extruders)
        :return: List of EMix object or None if parse is not successful

        NOTE:  To convert a template to a list of mixes for use with the ColorWheel object:
            mixes = list(map(EMix,parse_mixes("10/10/80,20/20/60,30/30/40, 40/40/20"))

        """
        results = MIXES_RE.findall(template)
        if results:
            emixes = list(map(EMix, results))
            for i in range(0, len(emixes)):
                emixes[i].tool_num = i

            return emixes

        return None

    # @staticmethod
    # def parse_relative(template: str, pf_layer_height: float=0.20):
    #     """
    #     Given a list of pairs, color (pattern index)
    #     and height (height of the band)
    #     a list of ColorBand objects is created as a
    #     stack for the ColorWheel generator.
    #
    #     :param template: string with two values delimited with '/' (ie pattern_index/height)
    #     :param pf_layer_height:
    #     :return: A list of ColorBands or empty array if the parse fails
    #     """
    #     rstack = []
    #     results = RELATIVE_MIX_RE.findall(template)
    #     layer_offset = 0
    #     if results:
    #         for result in results:
    #             pattern_idx = int(result[0])
    #             height = float(result[1])
    #             layer_count = int(height / pf_layer_height)
    #
    #             band = ColorBand(pi_layer_count=layer_count, pi_pattern_idx=pattern_idx, pi_start_layer=layer_offset)
    #             rstack.append(band)
    #             layer_offset += band.layer_count
    #     return rstack
    # </editor-fold  desc="RunValue Static Methods..."


class ColorGCoder:
    def __init__(self,
                 ps_infile_name: str,
                 ps_outfile_name: str = None):
        self.infile_name: str = ps_infile_name
        self.outfile_name: str = ps_outfile_name
        self.retraction_state = 0
        self.retraction_seen = 0.0
        self.in_layerchange = False

        if self.outfile_name is None:
            infile_path = PurePath(self.infile_name)
            self.outfile_name = str(infile_path.parent) + "\\" + infile_path.stem + "_colorized" + infile_path.suffix

        self.current = RunValues()
        self.metascan()
        self.colorize()

    # <editor-fold desc="ColorGCoder Methods...">
    def metascan(self, buf_size=8 * 1024):
        """
        Reading from the end of the file work backwards for three buffer sizes of text and
        extract and save all of the value-paris seen.

        In Slic3r this is necessary to obtain the layer_count (or build_height). Cura has
        a build height variable so this step is not necessary for a Cura file.

         NOTE: This is a post-processing script for  Slic3r. I don't use Cura except for
         testing but ColorBlender can be run from the command line and I have tested with
         a Cura generated file. To work seamlessly with Cura this code would have to be
         converted to a Cura plug-in.

        :param buf_size: Size in bytes to buffer.  Three buffers of text will be processed.
        :return:
        """
        file_size = os.path.getsize(self.infile_name)

        partline = ""

        fh = open(self.infile_name, mode='r+', encoding='utf-8')
        offset = 0
        remaining_size = min(buf_size * 3, file_size)  # Limit reverse search to 3 loops

        lookfor = {"LAYER_HEIGHT", "LAYER_COUNT", "retract_length"}
        found_count = 0

        while remaining_size > 0 and found_count < len(lookfor):
            offset = min(file_size, offset + buf_size)
            fh.seek(file_size - offset)

            buffer = fh.read(min(remaining_size, buf_size)) + partline
            start_offset = buffer.find("\n")

            if start_offset >= 0:  # eol seen
                partline = buffer[0:start_offset]
            else:
                start_offset = 0
                partline = ""

            remaining_size -= buf_size

            results = re.findall(FINDALL_MIX_RE.pattern, buffer[start_offset:], re.MULTILINE)
            if results:
                for val in results:
                    dname = val[0].strip()
                    self.current.val_dic[dname] = val[2].strip()
                    if dname in lookfor:
                        found_count += 1

        fh.close()

        # ###################################################################################################
    def colorize(self):
        """
        Process the input file line by line.

        :return:
        """
        try:
            # <editor-fold desc="open and initialize...">
            fin = open(file=self.infile_name, mode='r+', encoding='utf-8')

            fout = open(file=self.outfile_name, mode='w+', encoding='utf-8')

            line = " "
            infos = 0
            self.retraction_state = 0
            self.retraction_seen = 0.0
            self.in_layerchange = False
            # </editor-fold>

            while line:
                # start_fos = infos  # Save the offset to the start of the line
                line = fin.readline()

                self.current.line_count += 1
                infos += len(line)  # Keep track of the file offset

                self.process_line(line, fout)

            fin.close()
            fout.close()

        except (OSError, IOError) as e:
            # e = sys.exc_info()[0]
            print("Error: %s>" % e)

    def process_line(self, line, fout):
        """
        Give a line process as a sequential command to detect meta-data prior to Layer: 0
        and retractions and layer changes after Layer:0

        When a retraction is seen insert the 'Equal Mix' command and additional retraction / unretracts
        to keep the filament properly positioned.  Change the color mix according to the pattern / colorwheel .

        :param line: Line of gcode from the input stream
        :param fout: Output file handler
        :return:
        """

        # <editor-fold desc="Before Layer 0 - Intro Line ...">
        #######################################################
        # Prior to Layer 0 marker
        if self.current.cur_layer < 0:  # Still haven't seen the first 'LAYER_CHANGE'
            results = VALUES_RE.search(line)
            if results is None:
                fout.write(line)
                return

            # if results:  # Prior to Layer:0 keep any value pairs seen
            item_name = results.group(1).strip()
            item_value = results.group(2).strip()
            # self.val_dic[item_name] = item_value

            if item_name == "colorwheel_has_color" and item_value.lower() == "true":
                fout.write(line)

                self.current.load_if_present()
                if self.current.has_color:  # Successful load
                    self.current.generate_stack()
                    fout.write(self.current.mixes[self.current.pattern[0]].m165("Intro - Set Colormix"))

                return

            if item_name == "colorwheel_intro":
                fout.write(line)
                if self.current.has_color or self.current.load_if_present():

                    # self.current.generate_stack()
                    # fout.write(self.current.stack_report)
                    # fout.write(line)
                    fout.write(self.current.mixes[self.current.pattern[0]].m165("Intro - Set Colormix"))
                    cmds = list(results.group(2).split(","))
                    for cmd in cmds:  # Write out command from the value
                        fout.write(cmd.strip() + "\n")

                return

            elif item_name.startswith("colorwheel_"):
                self.current.val_dic[item_name] = item_value
                fout.write(line)
                return

            elif item_name == "Layer height":
                self.current.layer_height = float(item_value)
                # BUILD.layer_height = self.layer_height
                fout.write(line)
                return

            elif item_name == "LAYER_COUNT":
                if self.current.layer_count != int(item_value):
                    self.current.layer_count = int(item_value)
                    # BUILD.layer_count = self.current.layer_count
                    self.current.load_if_present()
                    # self.current.init_from_dict()
                    self.current.generate_stack()

                    # self.generate_stack()

                fout.write(line)
                return

            elif item_name == "tool_mixes":
                self.current.toolmix_values = item_value

            if item_name == "LAYER":
                fout.write(line)
                self.current.cur_layer = int(item_value)
                if not self.current.load_if_present():
                    return

                self.current.generate_stack()
                fout.write(self.current.stack_report)

                return

            fout.write(line)
            return

            # Handle the first Layer Identifier (should be LAYER:0}

        # </editor-fold>

        # <editor-fold desc="Layer Changes / Height ...">
        # #######################################################
        # results = LAYER_HEIGHT_RE.search(line)
        # if results :    # Layer change
        #     self.cur_layer += 1
        # else:
        if self.current.has_color is False:
            fout.write(line)
            return

        results = LAYER_NUM_RE.search(line)
        if results:  # Layer change
            layerval = int(results.group(1).strip())
            if self.current.cur_layer != layerval:
                self.current.cur_layer = layerval

                if self.current.pattern_index(self.current.cur_layer) != \
                        self.current.pattern_index(self.current.cur_layer - 1):  # Color change
                    fout.write(self.current.mixes[self.current.pattern[
                        self.current.pattern_index(self.current.cur_layer)]].m165("Color Change"))

            fout.write(line)
            return

        # </editor-fold>

        # <editor-fold desc="Retractions ...">
        #######################################################
        # Retract and Unretract

        #########################################################
        # After a retraction is seen look for the end in two ways
        #   retraction_state:
        #       0 = Clear, not withing a retraction
        #       1 = In a retraction looking for the end of the retraction statements
        #           to insert the additional filaments retraction statement
        #       2 = Retracted looking for an extrusion (unretract) to insert the
        #           additional filament unretract and set the colormix to the layer's color

        results = RETRACT_RE.search(line)  # Line has ' E-{}'
        if self.retraction_state == 0 and results:  # Start of a retraction section
            self.retraction_state = 1
            fout.write(self.current.m165_equal() +
                       line)

            if self.current.retract_length < 0.00:
                self.retraction_seen += float(results.group(1).strip())
            return

        if self.retraction_state == 1 and results:  # Continuation of retraction
            fout.write(line)
            if self.current.retract_length < 0.0:
                self.retraction_seen += float(results.group(1).strip())
            return

        if self.retraction_state == 1:  # Not a retraction so this is the end of the Wipe retractions
            self.retraction_state = 2  # Wipe is done, look for the Unretract (end of retraction section)
            if self.current.retract_length < 0.0:
                self.current.retract_length = self.retraction_seen
            fout.write(self.current.mixes[self.current.pattern_index(self.current.cur_layer)].second_retract(
                retract_length=-self.current.retract_length,
                comment="Retract for additional extruders"))

        if self.retraction_state == 2:  # Look for the end of the retraction section
            results = UNRETRACT_RE.search(
                line)  # Only look for 'E{}' within a 'retraction section' started by a ' E-{}'
            if results:  # Unretract seen
                self.retraction_state = 0  # Clear the retraction state
                mix = self.current.mixes[self.current.pattern[self.current.pattern_index(self.current.cur_layer)]]
                fout.write(line +
                           mix.second_retract(retract_length=self.current.retract_length,
                                              comment="Unretract for additional extruders") +
                           mix.m165("Restore/set layer color"))

                return
        fout.write(line)
        return

        # </editor-fold>

    # </editor-fold desc="ColorGCoder Methods...">


if __name__ == '__main__':
    # <editor-fold desc="Command Line Handler...">
    parser = argparse.ArgumentParser(description='script to filter the GCode output via customized rules')

    parser.add_argument('-v', '--verbose',
                        action="store_true",
                        help="verbose output")

    parser.add_argument('infile',
                        # type=argparse..FileType('r', encoding='utf-8'),
                        help="gcode file from slicer")

    parser.add_argument('-o', '--outfile',
                        help="filtered output - gcode file")

    args = parser.parse_args()

    pin_file = args.infile
    file_path = PurePath(pin_file)

    processed_file = str(file_path.parent) + "\\" + file_path.stem + "_processed" + file_path.suffix
    pout_file = args.outfile
    if pout_file is None:
        pout_filename = str(file_path.parent) + "\\" + file_path.stem + "_new" + file_path.suffix
    else:
        pout_filename = pout_file.name
    # </editor-fold>

    # <editor-fold desc="-> Main">
    # --------------------- Main ------------------------------

    print("Processing {}".format(pin_file))
    gcoder = ColorGCoder(ps_infile_name=pin_file, ps_outfile_name=pout_filename)

    # ------------------- End Main -----------------------------
    # </editor-fold desc="Main...">

    # <editor-fold desc="Cleanup...">

    if os.path.exists(processed_file):  # Prior run's file
        os.remove(processed_file)

    os.rename(pin_file, processed_file)
    os.rename(pout_filename, pin_file)
    os.remove(processed_file)
    # </editor-fold>

    print("Done\n")
