# ColorMix3R

Post processor written in Python.  Easily add color to a print using a mixing head extruder.  Can be seamlessly integrated in Slic3R or run from the command line (or batch) for Cura generated gcode (with the right settings).